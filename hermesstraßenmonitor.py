from time import sleep
import time
import os
import requests
import json
from Adafruit_CharLCD import Adafruit_CharLCD

# instantiate lcd and specify pins
lcd = Adafruit_CharLCD(rs=14, en=4,
                       d4=15, d5=18, d6=17, d7=22,
                       cols=16, lines=2)
lcd.clear()

rbl1 = '1633'
rbl2 = '1738'
api_key = os.environ['wl_api_key']
api_url = 'http://www.wienerlinien.at/ogd_realtime/monitor'
url = api_url + "?rbl=" + rbl1 + "&rbl=" + rbl2 + "&sender=" + api_key

while(True):
    lcd.set_cursor(0, 0)
    lcd.clear()
    lcd.message(time.strftime("%a,%d.%m.|%H:%M"))

    lcd.set_cursor(0, 1)
    response = requests.get(url)
    if response.status_code == requests.codes.ok:
        jsponse = response.json()
        infos = [None] * 2
        for monitor in jsponse['data']['monitors']:
            line = monitor['lines'][0]
            info = line['name'] + ' '
            for departure in line['departures']['departure'][:2]:
                info = info + str(departure['departureTime']['countdown']) + '/'

            info = info[:-1]
            if str(monitor['locationStop']['properties']['attributes']['rbl']) == rbl1:
                infos[0] = info
            else:
                infos[1] = info
        lcd.message(infos[0])
        lcd.set_cursor(8, 1)
        lcd.message(infos[1])
    lcd.home()
    sleep(5)

