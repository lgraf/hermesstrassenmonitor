from time import sleep
import time
import os
import requests
import json

rbl1 = '1633'
rbl2 = '1738'
api_key = os.environ['wl_api_key']
api_url = 'http://www.wienerlinien.at/ogd_realtime/monitor'
url = api_url + "?rbl=" + rbl1 + "&rbl=" + rbl2 + "&sender=" + api_key

while(True):
    response = requests.get(url)
    if response.status_code == requests.codes.ok:
        jsponse = response.json()
        infos = [None] * 2
        for monitor in jsponse['data']['monitors']:
            line = monitor['lines'][0]
            info = line['name'] + ' '
            for departure in line['departures']['departure'][:2]:
                info = info + str(departure['departureTime']['countdown']) + '/'

            info = info[:-1]
            if str(monitor['locationStop']['properties']['attributes']['rbl']) == rbl1:
                infos[0] = info
            else:
                infos[1] = info
        for info in infos:
            print info
    sleep(5)
